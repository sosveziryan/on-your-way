<?php
return array(
  // Header
  'header' => array(
    'bannerClass' => '',
    'footClass' => '',
    'img' => 'header-bg.png',
    'title' => '<span class = "bold-500">CREATE</span> NEW PURCHASE <span class = "bold-500">ORDER</span>',
  ),
  // Sections
  'sections' => array(
    array(
      'form' => array(
        'what' => array(
          'label' => 'What do you need to be delivered',
          'value' => 'iPhone 6',
        ),
        'image' => array(
          'label' => 'Item photo',
          'value' => array(
            array(
              'src' => 'iPhone.png',
              'alt' => 'Iphone',
            ),
          ),
          'placeholder' => '<i class = "icon-camera-outline font-accept"></i>',
        ),
        'info' => array(
          'label' => 'Additional information',
          'placeholder' => 'Additional information (link toward an item photo, color, size, etc.)',
        ),
        'price' => array(
          'label' => 'Price($)',
          'value' => 0,
        ),
        'qty' => array(
          'label' => 'Quantity',
          'value' => 1,
        ),
        'reward' => array(
          'title' => 'Reward for the traveler ($)',
          'options' => array(20,50,100),
          'value' => 100,
          'placeholder' => 'Or enter a custom amount',
        ),
        'from' => array(
          'label' => 'From',
          'placeholder' => 'Country'
        ),
        'to' => array(
          'label' => 'To',
          'placeholder' => 'City',
          'value' => 'Beirut, Lebanon',
        ),

        'preview' => array(
          'title' => 'Summary',
          'list' => array(
            array(
              'From:' => '--',
              'To:' => '--',
              'Quantity:' => '1',
              'Item:' => '--',
            ),
            array(
              'Item Price:' => '0$',
              'Traveler\'s reward:' => '0$',
              'On Your Way\'s fee:' => '0$',
            ),
            array(
              '<u>Total:</u>' => '0$',
            ),
          ),
        ),

        'submit' => 'Post',
      ),

    ),
  ),
);
?>
