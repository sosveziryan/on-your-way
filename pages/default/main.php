<main class = "main-content container">
  <form class="form-simple " action="index.html" method="post">
    <label class = "label">
      <span>From:</span>
      <input type="text" class = "input" placeholder = "Country" id = "googleAutoCompleteFrom">
    </label>
    <label class = "label">
      <span>To:</span>
      <input type="text" class="input" placeholder = "City" id = "googleAutoCompleteTo">
    </label>
    <label class="label">
      <button type="submit" class="input submit">Go</button>
    </label>
  </form>

  <table class = "content product-list" cellspacing = '0' cellpadding = '0' border = '0'>
    <thead>
      <tr>
        <th colspan="8" class="header" width = "1500">
          <h2>orders awaiting delivery</h2>
        </th>
      </tr>
      <tr>
        <th width = "184"></th>
        <th width = "479">What</th>
        <th width = "191">From</th>
        <th width = "86">To</th>
        <th width = "174">Traveler earns</th>
        <th width = "88">Price</th>
        <th width = "108">Quantity</th>
        <th width = "62"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <a href="#">
            <img src="<?= $img ?>product-1.jpg" alt="Aedle headpones">
          </a>
        </td>
        <td data-head = "What">Aedle headphones</td>
        <td data-head = "From">Anywhere</td>
        <td data-head = "To">Beirut</td>
        <td data-head = "Traveler earns">$60</td>
        <td data-head = "Price">$399.00</td>
        <td data-head = "Quantity">1</td>
        <td><a href="#" class = "input submit"><i class = "icon-mail"></i></a></td>
      </tr>
      <tr>
        <td>
          <a href="#">
            <img src="<?= $img ?>product-1.jpg" alt="Aedle headpones">
          </a>
        </td>
        <td data-head = "What">Aedle headphones</td>
        <td data-head = "From">Anywhere</td>
        <td data-head = "To">Beirut</td>
        <td data-head = "Traveler earns">$60</td>
        <td data-head = "Price">$399.00</td>
        <td data-head = "Quantity">1</td>
        <td><a href="#" class="input submit"><i class = "icon-mail"></i></a></td>
      </tr>
    </tbody>
  </table>

  <article class="article">
    <h4 class = "title">No item matching your trip?</h4>
    <p class = "content">we can notify youy when new matching items ape posted</p>
    <button type="button" name="button" class = "input submit">Notify me</button>
  </article>
</main>
