<main class = "main-content">

  <!-- Slider Section -->
  <?php $section = $sections[0]; ?>
  <section class = "p-t-sm-90">
    <div class="container">
      <?= $section['title'] ?>
      <div data-slider = "id-1111" class="wa-slider">
        <ul class = "nav">
          <li data-slide-to = "-1" class = "prev"></li>
          <li data-slide-to = "+1" class = "next"></li>
        </ul>
        <ul data-slides class = "slides">
          <?php foreach ($section['slides'] as $i => $slide): ?>
            <li data-slide class = "<?= $i ? '' : 'sliding' ?>">
              <figure>
                <img src="<?= $img . $slide['src'] ?>" alt="<?= $slide['alt'] ?>">
              </figure>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </section>

  <!-- Small Thumbnails Section -->
  <?php $section = $sections[1]; ?>
  <section>
    <div class="container p-t-30">
      <?= $section['title'] ?>
      <ul class = "wa-thumbs">
        <?php foreach ($section['list'] as $i => $row): ?>
          <li>
            <figure>
              <img src="<?= $img . $row['icon']['src'] ?>" alt="<?= $row['icon']['alt'] ?>">
              <figcaption>
                <h3><?= $row['title'] ?></h3>
                <p><?= $row['text'] ?></p>
              </figcaption>
            </figure>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </section>

  <!-- Posts Section -->
  <?php $section = $sections[2]; ?>
  <section>
    <div class="container p-t-sm-70">
      <h2></h2>
      <ul class = "posts">
        <?php foreach ($section['list'] as $i => $post): ?>
          <li>
            <figure>
              <div class="image-container">
                <img src="<?= $img . $post['img'] ?>" alt="<?= $post['alt'] ?>"/>
              </div>
              <figcaption class = "font-19">
                <h3><?= $post['title'] ?></h3>
                <?= $post['text'] ?>
              </figcaption>
            </figure>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </section>

  <!-- Preview Section -->
  <?php $section = $sections[3]; ?>
  <section class = "p-t-25 p-b-75 bg-cover" style="background-image: url(<?= $img . $section['img'] ?>);">
    <div class="container font-white">
      <?= $section['title'] ?>
      <div class="asided-block">
        <div>
          <div class="content editor">
            <?= $section['left'] ?>
          </div>
        </div>
        <?php $right = $section['right'] ?>
        <aside class="contact-cart">
          <!-- use before for ok-shield on left -->
          <figure>
            <div class="img-wrapper">
              <img src="<?= $img . $right['img'] ?>" alt="<?= $right['alt'] ?>">
            </div>
            <figcaption>
              <div class="stars"></div>
              <h3><?= $right['title'] ?></h3>
            </figcaption>
          </figure>
          <!-- Data -->
          <ul class = "bordered-list">
            <?php foreach ($right['data'] as $label => $count): ?>
              <li>
                <div><?= $count ?></div>
                <div><?= $label ?></div>
              </li>
            <?php endforeach; ?>
          </ul>

          <!-- Contacts -->
          <ul class = "contact-list">
            <?php foreach ($right['contacts'] as $j => $contact): ?>
              <li>
                <a href="<?= $contact['href'] ?>">
                  <?= $contact['icon'] ?> <?= $contact['label'] ?>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        </aside>
      </div>
    </div>
  </section>

  <!-- Review Section -->
  <?php $section = $sections[4]; ?>
  <section>
    <div class="container">
      <?= $section['title'] ?>
      <ul class = "reviews">
        <?php foreach ($section['reviews'] as $i => $li): ?>
          <li>
            <figure>
              <div class="image-wrapper">
                <img src="<?= $img . $li['img'] ?>" alt="<?= $li['title'] ?>">
              </div>
              <figcaption>
                <h3><?= $li['title'] ?></h3>
                <?= $li['text'] ?>
              </figcaption>
            </figure>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </section>

  <!-- Logos Section -->
  <?php $section = $sections[5]; ?>
  <section>
    <div class="container">
      <h2 class = "special-title"><span class = "font-thin"><?= $section['title'] ?></span></h2>
      <ul class = "logo-list">
        <?php foreach ($section['list'] as $i => $li): ?>
          <li>
            <figure>
              <figcaption><?= $li['title'] ?></figcaption>
              <img src="<?= $img . $li['src'] ?>" alt="<?= $li['alt'] ?>">
            </figure>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </section>

</main>
