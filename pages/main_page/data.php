<?php
return array(
  // Header
  'header' => array(
    'bannerClass' => 'huge',
    'img' => 'header-bg.png',
    'title' => '<h1 class = "font-liquid-medium">Get anything from <a href = "Amazon.com" class = "bold-500">Amazon.com</a> in Beirut</h1>',
    'text' => '<p class = "font-28 bold-500"><u class = "white">Delivered on demand by a traveller coming your way</u></p>',
    'footClass' => 'transparent',
    'left' => array(
      'title' => 'shop',
      'text' => 'What would you like to buy?',
    ),
    'right' => array(
      'title' => 'bring',
      'text' => 'Make money while traveling',
    ),
  ),
  'sections' => array(
    // Slider
    array(
      'title' => '<h3 class = "title">What our members are requesting</h3>',
      'slides' => array(
        array(
          'src' => 'slide-1.png',
          'alt' => 'notebook',
        ),
        array(
          'src' => 'slide-2.png',
          'alt' => 'malbert',
        ),
        array(
          'src' => 'slide-3.png',
          'alt' => 'wine',
        ),
        array(
          'src' => 'slide-3.png',
          'alt' => 'wine',
        ),
        array(
          'src' => 'slide-2.png',
          'alt' => 'wine',
        ),
      ),
    ),
    // small thumbnails
    array(
      'title' => '<h2 class = "title">How it works</h2>',
      'list' => array(
        array(
          'icon' => array(
            'src' => 'icon-pc.png',
            'alt' => 'PC',
          ),
          'title' => 'Post Your order',
          'text' => "Describe the item you want and add a delivery reward of your choice.\n Travelers will make offers to deliver it.",
        ),
        array(
          'icon' => array(
            'src' => 'icon-users.png',
            'alt' => 'PC',
          ),
          'title' => 'Choose a traveler',
          'text' => "Agree on delivery details and pre-pay for your order.\n We will hold your moneysecurely until your item is delivered.",
        ),
        array(
          'icon' => array(
            'src' => 'icon-ok.png',
            'alt' => 'PC',
          ),
          'title' => 'Collect Your item',
          'text' => "Meet with the traveler and pick up your item.\n We will transfer your payment to the traveler after you’ve confirmed a successful delivery.",
        ),
      ),
    ),
    // Posts
    array(
      'list' => array(
        0 => array(
          'img' => 'post-1.png',
          'alt' => 'post-1',
          'title' => 'On-hand support',
          'text' => "<p>From the first day you visit On Your Way to the day\n of the delivery, you can rely on the professional assistance from our Member Relations team, who are available to help you <b>24/7</b>.\n You can contact them <a href =\"#\"><span class = \"color-78 bold-400 italic\">here</span></a> or get in touch via <a href = \"Facebook\" ><span class = \"color-78 bold-400 italic\">Facebook</span></a></p>",
        ),
        1 => array(
          'img' => 'post-2.png',
          'alt' => 'post-2',
          'title' => 'Verified community',
          'text' => "<p>We moderate all profiles, photos, ratings, purchase requests and delivery offers in order to maintain trust\n and respect in the community.\n Users get in touch via our secure messaging system: you’ll know each other before the transaction\n and easily arrange a meet-up.</p>",
        ),
        2 => array(
          'img' => 'post-3.png',
          'alt' => 'post-3',
          'title' => 'Secure transactions',
          'text' => "<p>On Your Way is built as an escrow service: we hold money in deposit until the delivery is confirmed.\n This guarantees smooth transactions and protects\n the interests  of both parties. You can request with confidence since you will get an immediate 100% refund\n if your request is not fulfilled.</p>",
        ),
      ),
    ),
    // Preview
    array(
      'title' => '<h2 class = "title">Completely Safe</h2>',
      'img' => 'preview-bg.png',
      'left' => '<p>We verify all of our members’ profile by checking their email address, phone
        number and the validity of their bank details.<p>
        <p>We are also particularly careful when it comes down to legal issues: we monitor
        every purchase request posted on the platform and keep you informed of the
        latest legislation concerning the imports of goods for travelers.</p>
        <p>In a logic of transparency and to offer you the best chances to make great deals,
        members leave a review on their experience after each transaction.</p>',
      'right' => array(
        'title' => 'Liza Miller',
        'img' => 'girl-placeholder.png',
        'alt' => 'girl',
        'data' => array(
          'Trips' => '3',
          'Received Items' => '5',
          'Reviews' => '6',
        ),
        'contacts' => array(
          array(
            'icon' => '<i class = "icon-credit-card bg-purple"></i>',
            'label' => 'Credit Card',
            'href' => '#',
          ),
          array(
            'icon' => '<i class = "icon-vcard bg-orange"></i>',
            'label' => 'Identify',
            'href' => '#',
          ),
          array(
            'icon' => '<i class = "bg-red icon-scroll-a"></i>',
            'label' => 'E-mail',
            'href' => 'mail-to:',
          ),
        ),
      ),
    ),
    // Reviws
    array(
      'title' => '<h2 class = "title"><b class = " letter-spacing-1-5">Member reviews</b><h2>',
      'reviews' => array(
        array(
          'img' => 'review-1.png',
          'title' => 'Wasken A.',
          'text' => '"It\'s an amazing service, works perfectly and is an easy way to earn money!"',
        ),
        array(
          'img' => 'review-2.png',
          'title' => 'Liza M.',
          'text' => 'Quisque pretium est quam. Donec lectus ligula, ultrices non erat sit amet, suscipit dignissim arcu. Aenean at tincidunt magna.',
        ),
      ),
    ),
    // Logos
    array(
      'title' => '<u class = "black">they support us</u>',
      'list' => array(
        array(
          'title' => '',
          'src' => 'logo-1.png',
          'alt' => 'Taxi Cab',
        ),
        array(
          'title' => '',
          'src' => 'logo-2.png',
          'alt' => 'Taxi Cab',
        ),
        array(
          'title' => '',
          'src' => 'logo-3.png',
          'alt' => 'Taxi Cab',
        ),
        array(
          'title' => '<a class = "special-link">Read about our story in media</a>',
          'src' => 'logo-4.png',
          'alt' => 'Taxi Cab',
        ),
      ),
    ),
  ),
);
?>
