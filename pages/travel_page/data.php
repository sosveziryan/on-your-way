<?php
return array(
  // Header
  'header' => array(
    'bannerClass' => '',
    'img' => 'header-bg.png',
    'footClass' => '',
    'title' => '<h1 class = "up-c special"><b class = "bold-700">make</b> <b class = "bold-400">money</b> each time you <b class = "bold-700">travel</b> to <b class = "bold-700">Lebanon</b></h1>',
  ),
  // Sections
  'sections' => array(
    // Product List
    array(
      'form' => array(
        'from' => array(
          'label' => 'From:',
          'placeholder' => 'Country',
        ),
        'to' => array(
          'label' => 'To:',
          'placeholder' => 'City',
          'value' => 'Beirut, Lebanon',
        ),
        'submit' => 'GO',
      ),
      'title' => 'orders awayting delivery',
      'list' => array(
        array(
          'img' => 'product-1.png',
          'alt' => 'product-1',
          'label' => 'Canon EOS Rebel T5i DSLR Camer',
          'header' => array(
            'img' => 'product-1-author.png',
            'alt' => 'produt-author-1',
            'label' => 'Liza Mayer',
            'text' => 'is looking for',
          ),
          'right' => array(
            'label' => 'Reward',
            'content' => '150$',
          ),
          'content' => '<span>Deliver to</span> <i class = "icon-paper-plane font-accept"></i> <a class = " font-thin-300" href="google.map.com/london">London</a>',
          'footer' => array(
            'left' => array(
              'label' => 'Item price:',
              'text' => '808$',
            ),
            'right' => 'Make Offer',
          ),
        ),
        array(
          'img' => 'product-2.png',
          'alt' => 'product-2',
          'label' => 'Edgar and Lucy: A Novel',
          'header' => array(
            'img' => 'product-2-author.png',
            'alt' => 'produt-author-2',
            'label' => 'Thiago Lacerda',
            'text' => 'is looking for',
          ),
          'right' => array(
            'label' => 'Reward',
            'content' => '15$',
          ),
          'content' => '<span>Deliver to</span> <i class = "icon-paper-plane font-accept"></i> <a class = " font-thin-300" href="google.map.com/london">Beirut</a>',
          'footer' => array(
            'left' => array(
              'label' => 'Item price:',
              'text' => '10$',
            ),
            'right' => 'Make Offer',
          ),
        ),
        array(
          'img' => 'no-image.png',
          'alt' => 'product-3',
          'label' => 'Canon EOS Rebel T5i DSLR Camer',
          'header' => array(
            'img' => 'product-3-author.png',
            'alt' => 'produt-author-3',
            'label' => 'Liza Mayer',
            'text' => 'is looking for',
          ),
          'right' => array(
            'label' => 'Reward',
            'content' => '150$',
          ),
          'content' => '<span>Deliver to</span> <i class = "icon-paper-plane font-accept"></i> <a class = " font-thin-300" href="google.map.com/london">London</a>',
          'footer' => array(
            'left' => array(
              'label' => 'Item price:',
              'text' => '808$',
            ),
            'right' => 'Make Offer',
          ),
        ),
      ),
      'popup' => array(
        'images' => array(
          array(
            'src' => 'no-image.png',
            'alt' => 'img',
          ),
          array(
            'src' => 'empty.png',
            'alt' => 'img',
          ),
          array(
            'src' => 'empty.png',
            'alt' => 'img',
          ),
          array(
            'src' => 'empty.png',
            'alt' => 'img',
          ),
        ),
        'head' => array(
          'img' => 'popup-author.png',
          'label' => 'Liza Mayer',
          'text' => 'requested 8 hours ago',
          'left' => array(
            'Deliver to' => 'London',
            'From' => 'Anywhere',
            'Quantity: ' => '1',
            'item price:' => '<span class = "font-secondaryFont">808$</span>',
          ),
          'right' => array(
            'label' => 'Reward',
            'text' => '150$',
          ),
        ),
        'title' => 'Canon EOS Rebel T5i DSLR Camer',
        'text' => '<p class = "ls-05">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu quam nec est hendrerit
          euismod eget non mauris. Aliquam vulputate erat non condimentum tincidunt. Vivamus
          efficitur nisl molestie, bibendum diam quis, vestibulum ligula. Proin sit amet dapibus lorem, non
          auctor mauris. Vivamus at egestas orci.</p>',
        'button' => 'Make Offer',
      ),
    ),
    // Contact
    array(
      'title' => 'No item matching Your trip?',
      'text' => 'We can notify you when new matching items are posted.',
      'button' => 'Notify me',
    ),
  ),
);
?>
