<?php
$images = array(
  'slider' => array(
    array(
      'src' => 'notebook.png',
      'alt' => 'notebook',
    ),
    array(
      'src' => 'malbert.png',
      'alt' => 'malbert',
    ),
    array(
      'src' => 'wine.png',
      'alt' => 'wine',
    ),
  ),
);
$pages = array(

  'main_page' => array(
    'path' => 'pages/main_page/',
    'label' => 'Active Orders',
    'href' => '?page=main_page',
    'demoHref' => 'main_page_index.html',
  ),
  'shop' => array(
    'path' => 'pages/shop/',
    'label' => 'About us',
    'href' => '?page=shop',
    'demoHref' => 'shop_index.html',
  ),
  'travel_page' => array(
    'path' => 'pages/travel_page/',
    'label' => 'Sign in',
    'href' => '?page=travel_page',
    'demoHref' => 'travel_page_index.html',
  ),
  'main_iorie' => array(
    'path' => 'pages/main_iorie/',
    'label' => 'Sign up',
    'href' => '?page=main_iorie',
    'demoHref' => 'main_iorie_index.html',
  ),
);

$googleFonts = array(
  'Nunito Sans' => array(
    '200','200i','300','300i','400','400i','600','600i','700','700i','800','800i','900','900i',
  ),
  'Raleway' => array(
    '100','100i','200','200i','300','300i','400','400i','500','500i','600','600i','700','700i','800','800i','900','900i',
  ),
);

$currentPage = isset($_GET['page']) ? $_GET['page'] : 'main_page';
$pageFolder = $pages[$currentPage]['path'];
$img = $pageFolder . 'images/';
$popupUnicName = 'popup_unic_global_input_name';

/**
 * [Generate the google fonts]
 */
  $fonts = 'https://fonts.googleapis.com/css?family=';
  foreach($googleFonts as $name => $styles){

    str_replace(' ', '+', $name);
    $fonts .= "{$name}:";

    if(count($styles)){
      foreach ($styles as $styleName) {
        $fonts .= $styleName;
      }
    }else{
      for($i=1; $i < 10; $i++){
        $fonts .= ($i * 100) . ',' . ($i * 100) . 'i,';
      }
    }

    $fonts .= '|';
  }

  $fonts = substr($fonts, 0, -1);


// extracting the page data
extract(include($pageFolder . 'data.php'));


  $header['googleFonts'] = $fonts;

ob_start();
 ?>

<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body>
<!-- The all popups hider input -->
<input
  type="radio"
  name = "popup_unic_global_input_name"
  value=""
  class = "hide-all-popups"
  id = "hide-all-popups"
  checked="checked"
  >
<div class="pseudo-body">

<!-- Header -->
<?php include 'header.php'; ?>

<!-- Main content -->
<?php include $pageFolder . 'main.php'; ?>

<!-- Footer -->
<?php include 'footer.php'; ?>

</div>
</body>
</html>
<?php
$html = ob_get_clean();
$demoHtml = $html;
foreach ($pages as $i => $page) {
  $demoHtml = str_replace($page['href'], $page['demoHref'], $demoHtml);
}
file_put_contents($currentPage . '_index.html', $demoHtml);
echo $html;
 ?>
