<main class = "main-content container">
  <!-- Form Section -->
  <?php extract($sections[0]) ?>
  <section>
    <h2 class = "title"></h2>
    <?php extract($form) ?>
    <form class="form-simple" action="index.html" method="post">
      <!-- $what -->
      <label class = "label">
        <?= $what['label'] ?>
        <input class  = "input" type="text" value="<?= $what['value'] ?>" />
      </label>

      <!-- $image -->
      <div class="input-image-picker">
        <label class = "label">
          <?= $image['label'] ?>
          <input id = "image-input-11123" type="file" class  = "input">
        </label>
        <ul>
          <?php foreach ($image['value'] as $i => $li): ?>
            <li>
              <figure>
                <img src="<?= $img . $li['src'] ?>" alt="<?= $li['alt'] ?>"/>
              </figure>
              <i class="close icon-cancel"></i>
            </li>
          <?php endforeach; ?>
            <li>
              <figure>
                <label for = "image-input-11123">
                  <?= $image['placeholder'] ?>
                </label>
              </figure>
            </li>
        </ul>
      </div>

      <!-- $info -->
      <label class = "label">
        <?= $info['label'] ?>
        <textarea class  = "input" rows="3" placeholder = "<?= $info['placeholder'] ?>"></textarea>
      </label>

      <!-- $price -->
      <label class = "number-like-text label">
        <?= $price['label'] ?>
        <input class  = "input" type="number" value="<?= $price['value'] ?>"/>
      </label>

      <!-- $qty -->
      <label class = "number-small label">
        <?= $qty['label'] ?>
        <p>
          <i data-set-value = "-1">-</i>
          <input disabled = "disabled" type="number" value="<?= $qty['value'] ?>">
          <i data-set-value = "+1">+</i>
        </p>
      </label>

      <!-- $reward -->
      <div class="extended-radio">
        <label for="id_number" class = "label">
          <?= $reward['title'] ?>
        </label>
        <?php foreach ($reward['options'] as $i => $option): ?>
          <label class = "radio">
            <input
              type="radio"
              name="reward"
              value="<?= $option ?>"
              <?= ($option === $reward['value']) ? 'checked = "checked"' : ''; ?>
            />
            <div class="fake-radio"><?= $option ?></div>
          </label>
        <?php endforeach; ?>
        <input
          id ="id_number"
          type="number"
          name="reward"
          class  = "input"
          placeholder = "<?= $reward['placeholder'] ?>"
          value = "<?= $reward['value'] ?>"
        />
      </div>

      <!-- $from -->
      <label class = "label">
        <?= $from['label'] ?>
        <input class  = "input" type="text" placeholder = "<?= $from['placeholder'] ?>" />
      </label>

      <!-- $to -->
      <label class = "label">
        <?= $to['label'] ?>
        <input class  = "input" type="text" placeholder = "<?= $to['placeholder'] ?>"/>
      </label>

      <!-- $preview -->
      <?php extract($preview) ?>
      <div class="form-preview">
        <h3 class="title"><?= $title ?></h3>
        <ul>
          <?php foreach ($list as $i => $group): ?>
            <?php if ($i): ?>
              <li class = "placeholder"></li>
            <?php endif; ?>
            <?php foreach ($group as $label => $value): ?>
              <li>
                <span><?= $label ?></span>
                <span class = "right"><?= $value ?></span>
              </li>
            <?php endforeach; ?>
          <?php endforeach; ?>
        </ul>
      </div>

      <!-- $submit -->
      <footer>
        <input class = "input submit" type="submit" name="submit" value="<?= $submit ?>"/>
      </footer>
    </form>


  </section>
</main>
