<?php
return array(
  // Header
  'header' => array(
    'bannerClass' => '',
    'img' => 'header-bg.png',
    'footClass' => '',
    'title' => '<h1 class = "up-c"><b>make</b> money each time you <b>travel</b> to <b>Lebanon</b></h1>',
  ),
  // Sections
  'sections' => array(
    // Product List
    array(
      'form' => array(
        'from' => array(
          'label' => 'From:',
          'placeholder' => 'Country',
        ),
        'to' => array(
          'label' => 'To:',
          'placeholder' => 'City',
        ),
        'submit' => 'GO',
      ),
      'title' => 'orders awayting delivery',
      'list' => array(
        array(
          'img' => 'product-1.png',
          'alt' => 'product-1',
          'label' => 'Canon EOS Rebel T5i DSLR Camer',
          'header' => array(
            'img' => 'product-1-author.png',
            'alt' => 'produt-author-1',
            'label' => 'Liza Mayer',
            'text' => 'is looking for',
          ),
          'right' => array(
            'label' => 'Reward',
            'content' => '150$',
          ),
          'content' => '<span class = "font-thin">Deliver to</span> <i class = "icon-paper-plane font-accept"></i><a class = " font-thin-300" href="google.map.com/london">London</a>',
          'footer' => array(
            'left' => array(
              'label' => 'item price:',
              'text' => '808$',
            ),
            'right' => 'Make Offer',
          ),
        ),
        array(
          'img' => 'product-2.png',
          'alt' => 'product-2',
          'label' => 'Edgar and Lucy: A Novel',
          'header' => array(
            'img' => 'product-2-author.png',
            'alt' => 'produt-author-2',
            'label' => 'Thiago Lacerda',
            'text' => 'is looking for',
          ),
          'right' => array(
            'label' => 'Reward',
            'content' => '15$',
          ),
          'content' => '<span class = "font-thin">Deliver to</span> <i class = "icon-paper-plane font-accept"></i><a class = " font-thin-300" href="google.map.com/london">London</a>',
          'footer' => array(
            'left' => array(
              'label' => 'item price:',
              'text' => '10$',
            ),
            'right' => 'Make Offer',
          ),
        ),
        array(
          'img' => 'no-image.png',
          'alt' => 'product-3',
          'label' => 'Canon EOS Rebel T5i DSLR Camer',
          'header' => array(
            'img' => 'product-3-author.png',
            'alt' => 'produt-author-3',
            'label' => 'Liza Mayer',
            'text' => 'is looking for',
          ),
          'right' => array(
            'label' => 'Reward',
            'content' => '150$',
          ),
          'content' => '<span class = "font-thin">Deliver to</span> <i class = "icon-paper-plane font-accept"></i> <a class = " font-thin-300" href="google.map.com/london">London</a>',
          'footer' => array(
            'left' => array(
              'label' => 'item price:',
              'text' => '808$',
            ),
            'right' => 'Make Offer',
          ),
        ),
      ),
      'popup' => array(
        'images' => array(
          array(
            'src' => 'no-image.png',
            'alt' => 'img',
          ),
          array(
            'src' => 'empty.png',
            'alt' => 'img',
          ),
          array(
            'src' => 'empty.png',
            'alt' => 'img',
          ),
          array(
            'src' => 'empty.png',
            'alt' => 'img',
          ),
        ),
        'head' => array(
          'img' => 'popup-author.png',
          'label' => 'Liza Mayer',
          'text' => 'requested 8 hours ago',
          'left' => array(
            'Deliver to' => 'London',
            'From' => 'Anywhere',
            'Quantity: ' => '1',
            'item price:' => '808$',
          ),
          'right' => array(
            'label' => 'Reward',
            'text' => '150$',
          ),
        ),
        'title' => 'Cannon EOS Rebel T5i DSLR Camer',
        'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu quam nec est hendrerit
          euismod eget non mauris. Aliquam vulputate erat non condimentum tincidunt. Vivamus
          efficitur nisl molestie, bibendum diam quis, vestibulum ligula. Proin sit amet dapibus lorem, non
          auctor mauris. Vivamus at egestas orci. ',
        'button' => 'Make Offer',
      ),
    ),
    // Contact
    array(
      'title' => 'No item matching to your trip?',
      'text' => 'We can notify you when new matching items are posted.',
      'button' => 'Notify me',
    ),
  ),
);
?>
