<main class = "main-content">
  <!-- Products Section -->
  <?php extract($sections[0]); ?>
  <section>
    <div class="container-small">
      <!-- $form -->
      <?php extract($form); ?>
      <form method="post" class = "form-inline">
        <label class = "label">
          <?= $from['label'] ?>
          <input
            id="googleAutoCompleteFrom"
            class = 'input' type="text" placeholder = "<?= $from['placeholder'] ?>"/>
        </label>
        <label class = "label" >
          <?= $to['label'] ?>
          <input
            id="googleAutoCompleteTo"
            class = 'input' type="text" placeholder = "<?= $to['placeholder'] ?>"/>
        </label>
        <label class = "label">
          <input class = "submit input" type="submit" name="submit" value="<?= $submit ?>"/>
        </label>
      </form>

      <!-- $title -->
      <h2 class = "special-title font-mainFont p-t-25"><span class = "font-20"><?= $title ?></span></h2>

      <!-- $list -->
      <ul class = "p-t-25 cart-list">
        <?php foreach ($list as $i => $li): ?>
          <li>
            <figure>
              <div class="img-wrapper">
                <img src="<?= $img . $li['img'] ?>" alt="<?= $li['alt'] ?>"/>
              </div>
              <figcaption>
                <div class="content">
                  <div class="left">
                    <!-- Aouthor -->
                    <figure>
                      <div class="img-wrapper">
                        <img src="<?= $img . $li['header']['img'] ?>" alt="<?= $li['header']['alt'] ?>"/>
                      </div>
                      <figcaption>
                        <h4 class = "font-black"><?= $li['header']['label'] ?></h4>
                        <?= $li['header']['text'] ?>
                      </figcaption>
                    </figure>

                    <!-- Label -->
                    <h3><?= $li['label'] ?></h3>
                  </div>

                  <!-- Right Block -->
                  <div class="right">
                    <?= $li['right']['label'] ?>
                    <span><?= $li['right']['content'] ?></span>
                  </div>

                  <!-- Content -->
                  <p><?= $li['content'] ?></p>
                </div>


                <!-- Footer -->
                <footer>
                  <span class = "left">
                    <?= $li['footer']['left']['label'] ?>
                    <span class = "color-00 font-24"><?= $li['footer']['left']['text'] ?></span>
                  </span>
                  <label for="popup_make_offer" class = "right">
                    <?= $li['footer']['right'] ?>
                  </label>
                </footer>
              </figcaption>
            </figure>
          </li>
        <?php endforeach; ?>
      </ul>

      <!-- $popup -->
      <?php extract($popup); ?>
      <input
        class = "popup-trigger"
        name = "<?= $popupUnicName ?>"
        type="radio"
        id = "popup_make_offer"
      />
      <div class="popup">
        <label for="hide-all-popups"></label>
        <div class="content">
          <!-- Make Offer Block -->
          <div class="make-offer-block">
            <figure>
              <!-- Preview Images -->
              <div class=" image-container">
                <ul class = "preview-images-list">
                  <?php foreach ($images as $i => $image): ?>
                    <li>
                      <figure>
                        <img src="<?= $img . $image['src'] ?>" alt="<?= $image['alt'] ?>"/>
                      </figure>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
              <figcaption>
                <!-- Author info -->
                <figure class = "author">
                  <div class="image-wrapper">
                    <img src="<?= $img . $head['img'] ?>" alt="<?= $head['label'] ?>"/>
                  </div>
                  <figcaption>
                    <h4><?= $head['label'] ?></h4>
                    <p><?= $head['text'] ?></p>
                  </figcaption>
                </figure>

                <!-- Offer Info -->
                <ul class = "left">
                  <?php foreach ($head['left'] as $label => $value): ?>
                    <li>
                      <?= $label ?>
                      <span><?= $value ?></span>
                    </li>
                  <?php endforeach; ?>
                </ul>
                <aside class="right">
                  <div class="plane-sircle">
                    <?= $head['right']['text'] ?>
                  </div>
                  <p>
                    <?= $head['right']['label'] ?>
                  </p>
                </aside>
              </figcaption>
            </figure>
            <!-- Popup $title -->
            <h3><?= $title ?></h3>

            <!-- Popup $text -->
            <div class="editor">
              <?= $text ?>
            </div>

            <!-- Popup Footer -->
            <div class = "actions">
              <label for="hide-all-popups" >
                <span tabindex = "0" class = "input submit font-accept"><?= $button; ?></span>
              </label>
            </div>
          </div>
        </div>
      </div>

    </div>

  </section>

  <!-- Contact Section -->
  <?php extract($sections[1]); ?>
  <section>
    <div class="container contact-block">
      <h2 class = "head"><?= $title ?></h2>
      <p><?= $text ?></p>
      <a href="#"><?= $button ?></a>
    </div>
  </section>
</main>
