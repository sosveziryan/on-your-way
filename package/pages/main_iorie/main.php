<main class = "main-content">
  <!-- Slider Section -->
  <?php $section = $sections[0]; ?>
  <section class = "p-t-100">
    <div class="container">
      <h2 class = "title"><span><?= $section['title'] ?></span></h2>
      <div class="wa-slider" data-slider>
        <ul class = "nav">
          <li data-slide-to = "-1" class = "prev"></li>
          <li data-slide-to = "+1" class = "next"></li>
        </ul>
        <ul class = "slides" data-slides>
          <?php foreach ($section['slides'] as $i => $slide): ?>
            <li data-slide>
              <figure>
                <img src="<?= $img . $slide['src'] ?>" alt="<?= $slide['alt'] ?>">
                <figcaption>
                  <?= $slide['content'] ?>
                </figcaption>
              </figure>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </section>

  <!-- Small Thumbnails Section -->
  <?php $section = $sections[1]; ?>
  <section>
    <div class="container">
      <h2 class = "title"><?= $section['title'] ?></h2>
      <ul class = "wa-thumbs huge">
        <?php foreach ($section['list'] as $i => $row): ?>
          <li>
            <figure>
              <img src="<?= $img . $row['icon']['src'] ?>" alt="<?= $row['icon']['alt'] ?>">
              <figcaption>
                <h3><?= $row['title'] ?></h3>
                <p><?= $row['text'] ?></p>
              </figcaption>
            </figure>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </section>

  <!-- Posts Section -->
  <?php $section = $sections[2]; ?>
  <section>
    <div class="container p-b-75">
      <h2></h2>
      <ul class = "posts">
        <?php foreach ($section['list'] as $i => $post): ?>
          <li>
            <figure>
              <img src="<?= $img . $post['img'] ?>" alt="<?= $post['alt'] ?>"/>
              <figcaption>
                <h3><?= $post['title'] ?></h3>
                <?= $post['text'] ?>
              </figcaption>
            </figure>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </section>

</main>
