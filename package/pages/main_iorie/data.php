<?php
return array(
  // Header
  'header' => array(
    'bannerClass' => 'huge',
    'img' => 'header-bg.png',
    'title' => '<h1 class = "font-specialFont font-liquid-huge">Your Space In My Suitcase</h1>',
    'text' => "<p class = \"font-specialFont\">quisquam est qui dolorem ipsum quia dolor sit amet,\n consectetur, adipisci velit...</p>",
    'footClass' => '',
    'left' => array(
      'title' => 'shop',
      'text' => 'What would you like to buy?',
    ),
    'right' => array(
      'title' => 'bring',
      'text' => 'Make money while traveling',
    ),
  ),
  'sections' => array(
    // Slider
    array(
      'title' => 'Buy anything from anywhere, even if it’s not sold online',
      'slides' => array(
        array(
          'src' => 'slide-1.png',
          'alt' => 'iphone',
          'content' => '<h3 class = "font-white up-c"><b>GET YOURS DELIVERED FROM THE US</b></h3>
                        <p class = "font-white align-right">Starting from 399$</p>'
        ),
        array(
          'src' => 'slide-2.png',
          'alt' => 'ibook',
          'content' => '<h3 class = "font-white up-c"><b>GET YOURS DELIVERED FROM THE US</b><h3>
                        <p class = "font-white align-right">Starting from 399$</p>'
        ),
        array(
          'src' => 'slide-2.png',
          'alt' => 'ibook',
          'content' => '<h3 class = "font-white up-c"><b>GET YOURS DELIVERED FROM THE US</b><h3>
                        <p class = "font-white align-right">Starting from 399$</p>'
        ),
        array(
          'src' => 'slide-3.png',
          'alt' => 'ipad',
          'content' => '<h3 class = "font-white up-c"><b>GET YOURS DELIVERED FROM THE US</b><h3>
                        <p class = "font-white align-right">Starting from 399$</p>'
        ),

        array(
          'src' => 'slide-3.png',
          'alt' => 'ipad',
          'content' => '<h3 class = "font-white up-c"><b>GET YOURS DELIVERED FROM THE US</b><h3>
                        <p class = "font-white align-right">Starting from 399$</p>'
        ),
      ),
    ),
    // small thumbnails
    array(
      'title' => 'How it works',
      'list' => array(
        array(
          'icon' => array(
            'src' => 'icon-pc.png',
            'alt' => 'PC',
          ),
          'title' => 'Post Your order',
          'text' => "Describe the item you want and add a delivery reward of your choice.\n Travelers will make offers to deliver it.",
        ),
        array(
          'icon' => array(
            'src' => 'icon-ok.png',
            'alt' => 'PC',
          ),
          'title' => 'Choose a traveler',
          'text' => "Agree on delivery details and pre-pay for your order.\n We will hold your moneysecurely until your item is delivered.",
        ),
        array(
          'icon' => array(
            'src' => 'icon-users.png',
            'alt' => 'PC',
          ),
          'title' => 'Collect Your item',
          'text' => "Meet with the traveler and pick up your item.\n We will transfer your payment to the traveler after you’ve confirmed a successful delivery.",
        ),
      ),
    ),
    // Posts
    array(
      'list' => array(
        0 => array(
          'img' => 'post-1.png',
          'alt' => 'post-1',
          'title' => 'Safe transactions',
          'text' => "<p>You’ll receive an immediate refund\n if your item is not delivered.</p>",
        ),
        1 => array(
          'img' => 'post-2.png',
          'alt' => 'post-2',
          'title' => 'Unique items',
          'text' => "<p>Buy unique items that are not accessible\n locally or online.</p>",
        ),
        2 => array(
          'img' => 'post-3.png',
          'alt' => 'post-3',
          'title' => 'Better prices',
          'text' => "<p>Save on exorbitant shipping fees\n by paying our verified travelers.</p>",
        ),
      ),
    ),
  ),
);
?>
