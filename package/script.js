var initAutocomplete
(function($){
  var placeSearch, autocomplete;
  initAutocomplete = function () {
    var $from = $('#googleAutoCompleteFrom'),
    $to = $('#googleAutoCompleteTo'),
    $args = {types: ['geocode']};
    if ($to.length && $from.length ) {
      new google.maps.places.Autocomplete($from[0], $args);
      new google.maps.places.Autocomplete($to[0], $args );
    }
  }

  // Slider Functionality
  $(document).ready(function(){
    var $sliders = [];

    var slideAction = {
      '-1' : function($slider, $cb, cbArgs) {
        var $relocatingSlide = $slider['slides'].shift();
        $slider['slidesContainer'].append($relocatingSlide);
        $slider['slides'][0].addClass('sliding');
        $slider['slides'].push($relocatingSlide);
        $relocatingSlide.removeClass('sliding');
        if ($cb) {
          $cb((cbArgs) ? cbArgs : undefined);
        }
      },
      '+1' : function($slider, $cb, cbArgs) {
        var $relocatingSlide = $slider['slides'].pop();
        $slider['slides'][0].removeClass('sliding');
        $relocatingSlide.addClass('sliding');
        $slider['slidesContainer'].prepend($relocatingSlide);
        $slider['slides'].unshift($relocatingSlide);
        if ($cb) {
          $cb((cbArgs) ? cbArgs : undefined);
        }
      }
    };

    $('[data-slider]').each(function(sliderIndex){
      var $slider = $(this),
          $slides = [],
          $nav = [];
      // Slides
      $slider.find('[data-slides] [data-slide]').each(function(i, $slide, all){
        var $slide = $($slide);
        $slides.push($slide);
      });

      // Navigation
      $slider.find('[data-slide-to]').each(function(i, $navItem, all){
        var $navItem = $($navItem);
        $nav.push($navItem);
      });


      $slider['slides'] = $slides;
      $slider['slidesContainer'] = $( $slider.find('[data-slides]') );
      $slider['nav'] = $nav;
      $sliders.push($slider);
    });


    // Init events
    $sliders.forEach(function($slider){
      $slider['nav'].forEach(function($navItem){
        $navItem.click(function(){
          slideAction[$navItem[0]['dataset']['slideTo']]($slider);
        });
      });
    });
  });
})(jQuery);
