<footer class = "footer">
  <nav class = "footer-nav container">
    <div class="simple-list left">
      <ul>
        <li><a href="#" class = "link">FAQ</a></li>
        <li><a href="#" class = "link">Terms of use</a></li>
        <li>
          <label for="popup_1115" class = "link">Contact us</label>
          <input class = "popup-trigger" name = "<?= $popupUnicName ?>" type="radio" id = "popup_1115"/>
          <div class="popup">
            <label for="hide-all-popups"></label>
            <div class="content">
              <form class="form-special" action="index.html" method="post">
                <fieldset class = "main">
                  <legend>Contact us</legend>
                  <fieldset class = "content">
                    <header class = "editor">
                      <p class = "font-big align-right">HELLO?</p>
                      <p class = "align-right">
                        We are here to answer you! Get
                        in touch by writing to
                      </p>
                      <p class = "align-right">
                        <a href="#" class = "font-accept">support@onyourway.io</a>
                      </p>
                      <p class = "align-right">
                        or using the form below.
                      </p>
                    </header>
                    <label class = "label">
                      Name:
                      <input class = "input" type="text" name="" value="" />
                    </label>
                    <label class = "label">
                      E-mail:
                      <input class = "input" type="email" name="" value=""/>
                    </label>
                    <label class = "label">
                      Message:
                      <textarea class = "input"></textarea>
                    </label>
                    <fieldset class = "actions">
                      <label for="hide-all-popups">
                        <span tabindex = "0" class = "input" type="reset">Cancel</span>
                      </label>
                      <button class = "input submit" type="submit">Send</button>
                    </fieldset>
                  </fieldset>
                </fieldset>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class = "right">
      © On Your Way 2016
    </div>
  </nav>
</footer>
