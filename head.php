<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Travel to Lebanon and Earn Money</title>
  <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="style.css" rel="stylesheet">
  <script src = "jquery-3.2.1.min.js"></script>
  <script src = "script.js"></script>
  <script src = "scripts/image-picker.js"></script>
  <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOZBTcjinm_gNbmd-PR6M1ds3HREGKg2o&signed_in=true&libraries=places&callback=initAutocomplete"
    async defer
  ></script>
</head>
