<header class = "main-header" style="background-image: url(<?= $img . $header['img'] ?>)">
  <nav class="header-navigation">
    <figure class = "logo left">
      <a href="#">
        <img src="images/logo.png" alt="logo">
      </a>
    </figure>
    <div class="right nav-list">
      <ul class = "" tabindex="0">
        <?php foreach ($pages as $id => $li): ?>
          <li class="">
            <?php if ($currentPage === $id): ?>
              <span><?= $li['label'] ?></span>
            <?php else: ?>
              <a href = "<?= $li['href'] ?>"><?= $li['label'] ?></a>
            <?php endif; ?>
          </li>
        <?php endforeach; ?>
      </ul>
      <i class = "icon-menu-square-button font-accept"></i>
    </div>
  </nav>
  <div class="header-banner <?= $header['bannerClass'] ?>">
    <div class="content">
      <?= $header['title'] ?>
      <?php if (isset($header['text'])): ?>
        <?= $header['text'] ?>
      <?php endif; ?>
      <?php if (isset($header['left']) && isset($header['right'])): ?>
        <div class="pair-block <?= $header['footClass'] ?>">
          <a>
            <h4 class = "title"><?= $header['left']['title'] ?></h4>
            <p><?= $header['left']['text'] ?></p>
          </a>
          <a>
            <h4 class = "title"><?= $header['right']['title'] ?></h4>
            <p><?= $header['right']['text'] ?></p>
          </a>
        </div>
      <?php endif; ?>
    </div>
  </div>
</header>
