// (function($){
//     $(document).ready(function(){
//       // Image Picker
//       var $inputs = $('[data-image-picker]'),
//           buff = [];
//
//       function readURL(blob, $img){
//         var reader = new FileReader();
//         reader.onload = function (e) {
//           $img.attr('src', e.target.result);
//         }
//         reader.readAsDataURL(blob);
//       }
//
//       buffer = $inputs;
//       $inputs = [];
//       buffer.each(function(i, $input){
//         $inputs[i] = $(this);
//         $inputs[i]['sample'] = $($inputs[i].find('[data-sample]'));
//         $inputs[i]['placeholder'] = $($inputs[i].find('[data-placeholder]'));
//         $inputs[i]['list-block'] = $($inputs[i].find('[data-list-block]'));
//         $inputs[i]['input'] = $($inputs[i].find('input[type = "file"]'));
//         $inputs[i]['images'] = [];
//         $inputs[i]['files'] = [];
//       });
//
//       $inputs.forEach(function($input, i, $all){
//         $input['input'].change(function(){
//           if (this.files) {
//             for (var i = 0; i < this.files.length; i++) {
//               $file = this.files.item(i);
//               $inputs[i]['files'][] = $file;
//               $newImage = $input['sample'].copy();
//               $inputs[i]['images'][] = $newImage;
//               readURL($file, $newImage);
//             }
//           }
//         });
//
//       });
//     });
// })(jQuery);
